<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

use App\Model\Commision;

class CommisionController extends Controller
{
    public function item(int $id, Request $request) : JsonResponse
    {
    	try {
    		$model = Commision::findOrFail($id);
    	}

    	catch (\Exception $err){
    		logger($err->getMessage());

    		return response()->json(['status'=> false, 'message' => $err->getMessage(), 'model' => null ], 422);
    	}

    	return response()->json(['status' => true, 'model' => $model, 'message' => __('errors.item_success')], 200);
    }

    public function create(Request $request) : JsonResponse
    {
    	$model = new Commision;

    	try {
    		$model->fill([
    			'number' => $request->input('number'),
    			'city' => $request->input('city'),
    			'district' => $request->input('district'),
    			'area' => $request->input('area')
 		]);
    			$model->save();
    	}
    	catch (\Exception $err) {
    		logger($err->getMessage());

    		return response()->json(['status'=> false, 'message' => $err->getMessage(), 'model' => null ], 422);

    	}
    	return response()->json(['status' => true, 'model' => $model, 'message' => __('responses.create_success')], 200);
    }

    public function collection(Request $request) : JsonResponse
    {
        $params = $request->all();

        try {
        $all = Commision::select('id', 'lastname', 'name', 'patronname', 'birthday', 'address', 'phone','conviction');

        $all = $this->setPaginationQuery($all, $params)
            ->get();
        }
        catch (\Exception $err) {
            logger($err->getMessage());

            return response()->json(['status'=> false, 'message' => $err->getMessage(), 'collection' => [] ], 422);
        }
        return response()->json(['status' => true, 'collection' => $all, 'message' => __('responses.collection_success')], 200);

    }

    public function update(int $id, Request $request) : JsonResponse
    {

        $request->validate([
        	// unique:categories

            'number' => 'unique:commisions|string|required|max:255',
    		'city' => 'string|required|max:255',
    		'district' => 'string|required|max:255',
    		'area' => 'string|required|max:255'
     ]);

        try {
            $model = Commision::findOrFail($id);
        }

        catch (\Exception $err){
            logger($err->getMessage());

            return response()->json(['status'=> false, 'message' => $err->getMessage(), 'model' => null ], 422);
        }
        try {

            $model->fill($request->only('number', 'city', 'district', 'area'));
            $model->save();
        }
        catch (\Exception $err) {
            logger($err->getMessage());

            return response()->json(['status'=> false, 'message' => $err->getMessage(), 'model' => null ], 422);

        }
        return response()->json(['status' => true, 'model' => $model, 'message' => __('responses.update_success')], 200);
    }

    public function delete(int $id, Request $request) : JsonResponse
    {
        try {
            Commision::destroy($id);
        }
        catch (\Exception $err) {
            logger($err->getMessage());
            return response()->json(['status'=> false, 'message' => $err->getMessage(), 'model' => null ], 422);
        }
        return response()->json(['status' => true, 'model' => null, 'message' => __('responses.delete_success')], 200);
    } 
}
