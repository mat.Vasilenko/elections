<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    
    public function setPaginationQuery($q, array $params)
     {
      if (isset($params['start']) && isset($params['limit'])) {
         try {
          $q = $q->forPage($params['start'], $params['limit']);
         }
         catch (\Exception $err) {
          logger($err->getMessage());

          throw $err;
         }
      } else {
         try {
           $q = $q->limit(10);
         } catch (\Exception $err) {
           logger($err->getMessage());

           throw new $err;
         }
      }
      return $q;
     }
}
