<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

use App\Model\Human;

class HumanController extends Controller
{
    public function item(int $id, Request $request) : JsonResponse
    {
    	try {
    		$model = Human::findOrFail($id);
    	}

    	catch (\Exception $err){
    		logger($err->getMessage());

    		return response()->json(['status'=> false, 'message' => $err->getMessage(), 'model' => null ], 422);
    	}

    	return response()->json(['status' => true, 'model' => $model, 'message' => __('errors.item_success')], 200);
    }

    public function create(Request $request) : JsonResponse
    {
    	$model = new Human;

    	try {
    		$model->fill([
    			'lastname' => $request->input('lastname'),
    			'name' => $request->input('name'),
    			'patronname' => $request->input('patronname'),
    			'birthday' => $request->input('birthday'),
    			'address' => $request->input('address'),
    			'phone' => $request->input('phone'),
    			'conviction' => $request->input('conviction')
    		]);
    			$model->save();
    	}
    	catch (\Exception $err) {
    		logger($err->getMessage());

    		return response()->json(['status'=> false, 'message' => $err->getMessage(), 'model' => null ], 422);

    	}
    	return response()->json(['status' => true, 'model' => $model, 'message' => __('responses.create_success')], 200);
    }

    public function collection(Request $request) : JsonResponse
    {
        $params = $request->all();

        try {
        $all = Human::select('id', 'lastname', 'name', 'patronname', 'birthday', 'address', 'phone','conviction');

        $all = $this->setPaginationQuery($all, $params)
            ->get();
        }
        catch (\Exception $err) {
            logger($err->getMessage());

            return response()->json(['status'=> false, 'message' => $err->getMessage(), 'collection' => [] ], 422);
        }
        return response()->json(['status' => true, 'collection' => $all, 'message' => __('responses.collection_success')], 200);

    }

    public function update(int $id, Request $request) : JsonResponse
    {

        $request->validate([
        	// unique:categories

            'lastname' => 'string|required|max:255',
    		'name' => 'string|required|max:255',
    		'patronname' => 'string|required|max:255',
    		'birthday' => 'string|required|max:255',
    		'address' => 'string|required|max:255',
    		'phone' => 'string|required|max:255',
    		'conviction' => 'string|required|max:255'
        ]);

        try {
            $model = Human::findOrFail($id);
        }

        catch (\Exception $err){
            logger($err->getMessage());

            return response()->json(['status'=> false, 'message' => $err->getMessage(), 'model' => null ], 422);
        }
        try {

            $model->fill($request->only('id', 'lastname', 'name', 'patronname', 'birthday', 'address', 'phone','conviction'));
            $model->save();
        }
        catch (\Exception $err) {
            logger($err->getMessage());

            return response()->json(['status'=> false, 'message' => $err->getMessage(), 'model' => null ], 422);

        }
        return response()->json(['status' => true, 'model' => $model, 'message' => __('responses.update_success')], 200);
    }

    public function delete(int $id, Request $request) : JsonResponse
    {
        try {
            Human::destroy($id);
        }
        catch (\Exception $err) {
            logger($err->getMessage());
            return response()->json(['status'=> false, 'message' => $err->getMessage(), 'model' => null ], 422);
        }
        return response()->json(['status' => true, 'model' => null, 'message' => __('responses.delete_success')], 200);
    } 
}
