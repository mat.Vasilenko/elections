<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PhotosController extends Controller
{
    public function item(int $id, Request $request) : JsonResponse
    {
    	try {
    		$model = Photo::findOrFail($id);
    	}

    	catch (\Exception $err){
    		logger($err->getMessage());

    		return response()->json(['status'=> false, 'message' => $err->getMessage(), 'model' => null ], 422);
    	}

    	return response()->json(['status' => true, 'model' => $model, 'message' => __('errors.item_success')], 200);
    }

    public function create(Request $request) : JsonResponse
    {
    	$model = new Photo;

    	try {
    		$model->fill([
    			'path' => $request->input('path')
   		]);
    			$model->save();
    	}
    	catch (\Exception $err) {
    		logger($err->getMessage());

    		return response()->json(['status'=> false, 'message' => $err->getMessage(), 'model' => null ], 422);

    	}
    	return response()->json(['status' => true, 'model' => $model, 'message' => __('responses.create_success')], 200);
    }

    public function collection(Request $request) : JsonResponse
    {
        $params = $request->all();

        try {
        $all = Photo::select('path');

        $all = $this->setPaginationQuery($all, $params)
            ->get();
        }
        catch (\Exception $err) {
            logger($err->getMessage());

            return response()->json(['status'=> false, 'message' => $err->getMessage(), 'collection' => [] ], 422);
        }
        return response()->json(['status' => true, 'collection' => $all, 'message' => __('responses.collection_success')], 200);

    }

    public function update(int $id, Request $request) : JsonResponse
    {

        $request->validate([
        	// unique:categories

    		'path' => 'string|required|max:255',
        ]);

        try {
            $model = Photo::findOrFail($id);
        }

        catch (\Exception $err){
            logger($err->getMessage());

            return response()->json(['status'=> false, 'message' => $err->getMessage(), 'model' => null ], 422);
        }
        try {

            $model->fill($request->only('path'));
            $model->save();
        }
        catch (\Exception $err) {
            logger($err->getMessage());

            return response()->json(['status'=> false, 'message' => $err->getMessage(), 'model' => null ], 422);

        }
        return response()->json(['status' => true, 'model' => $model, 'message' => __('responses.update_success')], 200);
    }

    public function delete(int $id, Request $request) : JsonResponse
    {
        try {
            Photo::destroy($id);
        }
        catch (\Exception $err) {
            logger($err->getMessage());
            return response()->json(['status'=> false, 'message' => $err->getMessage(), 'model' => null ], 422);
        }
        return response()->json(['status' => true, 'model' => null, 'message' => __('responses.delete_success')], 200);
    } 
}
