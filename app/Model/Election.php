<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use Maatwebsite\Excel\Concerns\FromCollection;


class Election extends Model
{

	public function collection()
    {
        return Election::all();
    }
    
    protected $table = 'elections';

    protected $fillable = [
        'id','name'
    ];
}
