<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HumanData extends Model
{
    protected $table = 'humans_data';
}
