<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Human extends Model
{
    protected $table = 'humans';
}
