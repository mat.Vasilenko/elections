<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('/')
	// ->middleware('auth:api')
	->group(function(){

		Route::prefix('/form')
			->group(function(){

				Route::get('/{id}', 'FormController@item');
				Route::get('/', 'FormController@collection');
				Route::post('/', 'FormController@create');
				Route::patch('/{id}', 'FormController@update');
				Route::delete('/{id}', 'FormController@delete');
			});

		Route::prefix('/human')
			->group(function(){

				Route::get('/{id}', 'HumanController@item');
				Route::get('/', 'HumanController@collection');
				Route::post('/', 'HumanController@create');
				Route::patch('/{id}', 'HumanController@update');
				Route::delete('/{id}', 'HumanController@delete');
			});

		Route::prefix('/commision')
			->group(function(){
				Route::get('/{id}', 'CommisionController@item');
				Route::get('/', 'CommisionController@collection');
				Route::post('/', 'CommisionController@create');
				Route::patch('/{id}', 'CommisionController@update');
				Route::delete('/{id}', 'CommisionController@delete');

			});
		Route::prefix('/election')
			->group(function(){
				Route::get('/900', 'ElectionController@export');				
				// Route::get('/{id}', 'ElectionController@item');
				Route::get('/', 'ElectionController@collection');
				Route::post('/', 'ElectionController@create');
				Route::patch('/{id}', 'ElectionController@update');
				Route::delete('/{id}', 'ElectionController@delete');
			});

		Route::prefix('/position')
			->group(function(){
				Route::get('/{id}', 'PositionController@item');
				Route::get('/', 'PositionController@collection');
				Route::post('/', 'PositionController@create');
				Route::patch('/{id}', 'PositionController@update');
				Route::delete('/{id}', 'PositionController@delete');
			});

		Route::prefix('/photo')
			->group(function(){
				Route::get('/{id}', 'PhotoController@item');
				Route::get('/', 'PhotoController@collection');
				Route::post('/', 'PhotoController@create');
				Route::patch('/{id}', 'PhotoController@update');
				Route::delete('/{id}', 'PhotoController@delete');
			});

		Route::prefix('/stasus')
			->group(function(){
				Route::get('/{id}', 'StatusController@item');
				Route::get('/', 'StatusController@collection');
				Route::post('/', 'StatusController@create');
				Route::patch('/{id}', 'StatusController@update');
				Route::delete('/{id}', 'StatusController@delete');
			});

	});