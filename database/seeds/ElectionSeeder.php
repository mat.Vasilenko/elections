<?php

use Illuminate\Database\Seeder;

class ElectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Model\Election::create([
        	'name' => 'Местные выборы 2015'
        ]);

         App\Model\Election::create([
        	'name' => 'Выборы президента 2014'
        ]);
    }
}
